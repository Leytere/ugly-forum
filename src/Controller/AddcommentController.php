<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

use App\Entity\Post;
use App\Entity\Comment;

class AddcommentController extends AbstractController
{
    /**
     * @Route("/addcomment/{id}", name="addcomment")
     */
    public function index(ObjectManager $mana, Request $req, $id)
    {
        $postRep = $this->getDoctrine()->getRepository(Post::class);
        $post = $postRep->getById($id);

        if ($req->getMethod() === 'POST') {
            $message = $req->request->get('message');
            if (!empty($message)) {

                $comment = new Comment();
                $comment->setMessage($message)
                    ->setDate(new \DateTime('now', (new \DateTimeZone('Europe/Madrid'))))
                    ->setPost($post)
                    ->setAuthor($this->getUser());
                $mana->persist($comment);
                $mana->flush();

                return $this->redirectToRoute('showpost', ['id' => $post->getId()]);
            }
        }

        return $this->render('addcomment/index.html.twig', [
            'post' => $post,
        ]);
    }
}
