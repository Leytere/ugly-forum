<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

use App\Entity\User;
use App\Entity\Post;
use App\Entity\Comment;

class AddpostController extends AbstractController
{
    /**
     * @Route("/addpost", name="addpost")
     */
    public function index(Request $req, ObjectManager $mana)
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $this->getUser();

        if ($req->getMethod() === 'POST') {
            $title = $req->request->get('title');
            $message = $req->request->get('message');

            if (!empty($title) && !empty($message)) {

                $post = new Post();
                $post->setTitle($title);
                $post->setMessage('bloup');
                $post->setAuthor($user);
                $post->setDate(new \DateTime('now', (new \DateTimeZone('Europe/Madrid'))));
                $mana->persist($post);

                $comment = new Comment();
                $comment->setAuthor($user);
                $comment->setDate(new \DateTime('now', (new \DateTimeZone('Europe/Madrid'))));
                $comment->setPost($post);
                $comment->setMessage($message);
                $mana->persist($comment);

                $mana->flush();
                $this->addFlash('success', 'Post Added');
                return $this->redirectToRoute('showpost', ['id' => $post->getId()]);
            }
        }

        return $this->render('addpost/index.html.twig', [
            'controller_name' => 'AddpostController',
        ]);
    }
}
