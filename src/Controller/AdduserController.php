<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use App\Entity\User;

class AdduserController extends AbstractController
{
    /**
     * @Route("/adduser", name="adduser")
     */
    public function index(Request $req, ObjectManager $mana, UserPasswordEncoderInterface $encoder)
    {
        if ($req->getMethod() === 'POST') {
            $username = $req->request->get('username');
            $pass = $req->request->get('pass');
            $email = $req->request->get('email');

            if (!empty($username) && !empty($pass) && !empty($email)) {

                $user = new User();
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setPassword($encoder->encodePassword($user, $pass));

                $mana->persist($user);
                $mana->flush();

                $this->addFlash('success', 'User Added'); 
                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render('adduser/index.html.twig');
    }
}
