<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;

class DeleteController extends AbstractController
{
    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function index(ObjectManager $mana, $id)
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->getById($id);
        $mana->remove($user);
        $mana->flush();

        return $this->redirectToRoute('app_logout');
    }
}
