<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;

class DeluserController extends AbstractController
{
    /**
     * @Route("/deluser", name="deluser")
     */
    public function index(Request $req, ObjectManager $mana)
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $users = $repo->getAll();

        if ($req->getMethod() === 'POST') {
            $userId = $req->request->get('user');
            $user = $repo->getById($userId);
            $username = $user->getUsername();
            $mana->remove($user);
            $mana->flush();
            $this->addFlash('success', $username." has been deleted.");
        }

        return $this->render('deluser/index.html.twig', [
            'users' => $users,
        ]);
    }
}
