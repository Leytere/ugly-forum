<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Post;

class ShowpostsController extends AbstractController
{
    /**
     * @Route("/showposts", name="showposts")
     */
    public function index()
    {
        $postRep = $this->getDoctrine()->getRepository(Post::class);
        $posts = $postRep->getAll();

        return $this->render('showposts/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     *  @Route("/showposts/{id}", name="showpost")
     */
    public function showPost($id) {
        
        $postRep = $this->getDoctrine()->getRepository(Post::class);
        $post = $postRep->getById($id);
        $comments = $post->getComments();

        return $this->render('showposts/showpost.html.twig', ['post' => $post, 'comments' => $comments]);
    }
}
