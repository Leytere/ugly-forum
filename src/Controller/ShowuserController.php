<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\User;

class ShowuserController extends AbstractController
{
    /**
     * @Route("/showuser/{id}", name="showuser")
     */
    public function index($id)
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->getById($id);
        $comments = $user->getComments();
        $posts = $user->getPosts();

        return $this->render('showuser/index.html.twig', [
            'user' => $user,
            'comments' => $comments,
            'posts' => $posts
        ]);
    }
}
