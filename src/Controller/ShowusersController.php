<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\User;

class ShowusersController extends AbstractController
{
    /**
     * @Route("/showusers", name="showusers")
     */
    public function index(Request $req)
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $users = $repo->getAll();

        return $this->render('showusers/index.html.twig', [
            'users' => $users,
        ]);
    }
}
